const express = require('express');
const app = express();
var sql = require("mssql");
const pool = require("./connectDB.js");

var reportQueries = require("./reportsQueries");
var tableLayouts = require("./reportTableLayout");
app.get("/", async (req, res) => {
    await pool.connect();
    
    var request = pool.request();
    var res1 = await request.query(reportQueries.MonthlyForecast);
    var output1 = tableLayouts.showMonthlyReport(res1);

    var res2 = await request.query(reportQueries.Forward3Forecast);
    var output2 = tableLayouts.showF3Report(res2);

    var res3 = await request.query(reportQueries.FullYearForecast);
    var output3 = tableLayouts.showFullYearReport(res3);

    res.send(output1 + output2 + output3);

    pool.close();
});

var importPSData = require("./importDataScript");
app.get("/importPSData", async (req, res) => {
    importPSData();
    res.send("Importing data, check console log for complete done");
});


//========== TESTING ROUTES =========================
app.get("/hotels", async function(req, res) {
    // bad practice to keep connecting but just leave it for now
    await pool.connect();
    
    var request = pool.request();
    var result = await request.query("Select Name, HotelId FROM Hotels");
    res.send(result.recordset);
    pool.close();
});

var initializeHotels = require("./initializeHotels");
app.get("/initializeHotels", async (req, res) => {
    var hotels = await initializeHotels();
    res.send(hotels);
});

app.listen(3000, function() {
    console.log("Server started!");
});

