var _ = require("underscore");
const xlsxFile = require('read-excel-file/node');
var saveFuncs = require('./saveDataScript.js');
var sql = require("mssql");
const pool = require("./connectDB.js");

var currentHotels = [];
var dataObj = {};

// check current list of hotels to see if there are new ones to add
function checkForNewHotels(hotelList) {
    // can we assume that all reports will have same number of hotels? => NO (ATM and RoomStats have less)
    // difference returns array of vals from hotelList that are not in currentHotels (shallow comparison!)
    var currentHotelsNames = _(currentHotels).map((hotel) => {
        return hotel.Name;
    });

    var newHotels = _(hotelList).difference(currentHotelsNames);
    if (newHotels.length) addHotels(newHotels);
}

function addHotels(newHotels) {
    console.log("Need to add new hotels: ");
    console.log(newHotels);
    _(newHotels).each(async (hotel) => {
        await pool.connect();
        var request = pool.request();
        request.input("Name", sql.NVarChar, hotel.Name);
        var result = await request.query("Insert Into Hotels (Name) OUTPUT INSERTED.Name, INSERTED.HotelId Values(@Name)");
        currentHotels.push(result.recordset);
    });
}

function transformData(rows, sheetName) {
    var hotelList = [];
    rows.forEach((row) => {
        var nameValue = row[0];
        // Filter out the ones as plans -> option to do it in scraping
        var regex1 = RegExp("^[x][A-Z]");
        var regex2 = RegExp("^Temp[0-9]");
        var regex3 = RegExp("^Temp-");
        if (!regex1.test(nameValue) && !regex2.test(nameValue) && !regex3.test(nameValue) && nameValue != 'Total') {
            hotelList.push(nameValue);
        }
    });

    // Want to do for each sheet because can't assume that each PS export will have the same hotels (rare case that they don't)
    checkForNewHotels(hotelList);
    
    // Transforming the data from import to dataobj => might be implemented differently with real extraction method
    switch(sheetName) {
        case 'MTD_AM_Stats':
            dataObj[sheetName] = _(rows).map((row) => {
                return {"HotelName": row[0], "TotalRev": row[1], "GOP": row[8], "NOI": row[16]}
            });
            break;
        case 'MTD_Room_Stats':
            dataObj[sheetName] = _(rows).map((row) => {
                return {"HotelName": row[0], "Occ": row[3], "ADR": row[4], "RevPar": row[5]}
            });
            break;
        case 'YTD_AM_Stats':
            dataObj[sheetName] = _(rows).map((row) => {
                return {"HotelName": row[0], "TotalRev": row[1], "GOP": row[8], "NOI": row[15]}
            });
            break;
        case 'YTD_Room_Stats':
            dataObj[sheetName] = _(rows).map((row) => {
                return {"HotelName": row[0], "Occ": row[3], "ADR": row[4], "RevPar": row[5]}
            });
            break;
        default:
            dataObj[sheetName] = _(rows).map((row) => {
                return {"HotelName": row[0], "PrimaryForecast": row[11], "PrevWeekForecast": row[10]}
            });                    
    }
}

// go through each hotel obj and save the metrics data to corresponding table
async function saveData() {
    // Report Obj -> make sure to save this first since foreign key to other tables
    var reportObj = {SourceName: "ProfitSword", ExtractDate: new Date(), PreviousSnapshotWeek: new Date("6/14/2020")}
    var reportId;
    await saveFuncs.saveReportObj(reportObj).then(value => {
        reportId = value[0].ReportId;
        console.log("New Report:" + reportId);
    });

    for (var hotelName in currentHotels) {
        var hotel = currentHotels[hotelName];
       // Occupancy
       var occObj = { 
            HotelId: hotel.HotelId, 
            ReportId: reportId, 
            MTD: hotel.MTD_Room_Stats.Occ, 
            YTD: hotel.YTD_Room_Stats.Occ, 
            FullMonthForecast: hotel.Monthly_Occ.PrimaryForecast,
            FullMonthPWForecast: hotel.Monthly_Occ.PrevWeekForecast,
            FullMonthPWVariance: (hotel.Monthly_Occ.PrimaryForecast - hotel.Monthly_Occ.PrevWeekForecast) / hotel.Monthly_Occ.PrevWeekForecast,
            F3Forecast: hotel.F3_Occ.PrimaryForecast,
            F3PWForecast: hotel.F3_Occ.PrevWeekForecast,
            F3PWVariance: (hotel.F3_Occ.PrimaryForecast - hotel.F3_Occ.PrevWeekForecast) / hotel.F3_Occ.PrevWeekForecast,
            FullYearForecast: hotel.FullYear_Occ.PrimaryForecast,
            FullYearPWForecast: hotel.FullYear_Occ.PrevWeekForecast,
            FullYearPWVariance: (hotel.FullYear_Occ.PrimaryForecast - hotel.FullYear_Occ.PrevWeekForecast) / hotel.FullYear_Occ.PrevWeekForecast
        }

        await saveFuncs.saveOccupancyObj(occObj);

        // ADR
       var adrObj = { 
            HotelId: hotel.HotelId, 
            ReportId: reportId, 
            MTD: hotel.MTD_Room_Stats.ADR, 
            YTD: hotel.YTD_Room_Stats.ADR, 
            FullMonthForecast: hotel.Monthly_ADR.PrimaryForecast,
            FullMonthPWForecast: hotel.Monthly_ADR.PrevWeekForecast,
            FullMonthPWVariance: (hotel.Monthly_ADR.PrimaryForecast - hotel.Monthly_ADR.PrevWeekForecast) / hotel.Monthly_ADR.PrevWeekForecast,
            F3Forecast: hotel.F3_ADR.PrimaryForecast,
            F3PWForecast: hotel.F3_ADR.PrevWeekForecast,
            F3PWVariance: (hotel.F3_ADR.PrimaryForecast - hotel.F3_ADR.PrevWeekForecast) / hotel.F3_ADR.PrevWeekForecast,
            FullYearForecast: hotel.FullYear_ADR.PrimaryForecast,
            FullYearPWForecast: hotel.FullYear_ADR.PrevWeekForecast,
            FullYearPWVariance: (hotel.FullYear_ADR.PrimaryForecast - hotel.FullYear_ADR.PrevWeekForecast) / hotel.FullYear_ADR.PrevWeekForecast
        }

        await saveFuncs.saveADRObj(adrObj);

        // RevPar
       var revParObj = { 
            HotelId: hotel.HotelId, 
            ReportId: reportId, 
            MTD: hotel.MTD_Room_Stats.RevPar, 
            YTD: hotel.YTD_Room_Stats.RevPar, 
            FullMonthForecast: hotel.Monthly_RevPar.PrimaryForecast,
            FullMonthPWForecast: hotel.Monthly_RevPar.PrevWeekForecast,
            FullMonthPWVariance: (hotel.Monthly_RevPar.PrimaryForecast - hotel.Monthly_RevPar.PrevWeekForecast) / hotel.Monthly_RevPar.PrevWeekForecast,
            F3Forecast: hotel.F3_RevPar.PrimaryForecast,
            F3PWForecast: hotel.F3_RevPar.PrevWeekForecast,
            F3PWVariance: (hotel.F3_RevPar.PrimaryForecast - hotel.F3_RevPar.PrevWeekForecast) / hotel.F3_RevPar.PrevWeekForecast,
            FullYearForecast: hotel.FullYear_RevPar.PrimaryForecast,
            FullYearPWForecast: hotel.FullYear_RevPar.PrevWeekForecast,
            FullYearPWVariance: (hotel.FullYear_RevPar.PrimaryForecast - hotel.FullYear_RevPar.PrevWeekForecast) / hotel.FullYear_RevPar.PrevWeekForecast
        }

        await saveFuncs.saveRevParObj(revParObj);

        // TotalRev
       var totalRevObj = { 
            HotelId: hotel.HotelId, 
            ReportId: reportId, 
            MTD: hotel.MTD_AM_Stats ? hotel.MTD_AM_Stats.TotalRev : null, 
            YTD: hotel.YTD_AM_Stats? hotel.YTD_AM_Stats.TotalRev : null, 
            FullMonthForecast: hotel.Monthly_TotalRev.PrimaryForecast,
            FullMonthPWForecast: hotel.Monthly_TotalRev.PrevWeekForecast,
            FullMonthPWVariance: (hotel.Monthly_TotalRev.PrimaryForecast - hotel.Monthly_TotalRev.PrevWeekForecast) / hotel.Monthly_TotalRev.PrevWeekForecast,
            F3Forecast: hotel.F3_TotalRev.PrimaryForecast,
            F3PWForecast: hotel.F3_TotalRev.PrevWeekForecast,
            F3PWVariance: (hotel.F3_TotalRev.PrimaryForecast - hotel.F3_TotalRev.PrevWeekForecast) / hotel.F3_TotalRev.PrevWeekForecast,
            FullYearForecast: hotel.FullYear_TotalRev.PrimaryForecast,
            FullYearPWForecast: hotel.FullYear_TotalRev.PrevWeekForecast,
            FullYearPWVariance: (hotel.FullYear_TotalRev.PrimaryForecast - hotel.FullYear_TotalRev.PrevWeekForecast) / hotel.FullYear_TotalRev.PrevWeekForecast
        }

        await saveFuncs.saveTotalRevObj(totalRevObj);

        // GOP
        var gopObj = { 
            HotelId: hotel.HotelId, 
            ReportId: reportId, 
            MTD: hotel.MTD_AM_Stats ? hotel.MTD_AM_Stats.GOP : null, 
            YTD: hotel.YTD_AM_Stats ? hotel.YTD_AM_Stats.GOP : null, 
            FullMonthForecast: hotel.Monthly_GOP.PrimaryForecast,
            FullMonthPWForecast: hotel.Monthly_GOP.PrevWeekForecast,
            FullMonthPWVariance: (hotel.Monthly_GOP.PrimaryForecast - hotel.Monthly_GOP.PrevWeekForecast) / hotel.Monthly_GOP.PrevWeekForecast,
            F3Forecast: hotel.F3_GOP.PrimaryForecast,
            F3PWForecast: hotel.F3_GOP.PrevWeekForecast,
            F3PWVariance: (hotel.F3_GOP.PrimaryForecast - hotel.F3_GOP.PrevWeekForecast) / hotel.F3_GOP.PrevWeekForecast,
            FullYearForecast: hotel.FullYear_GOP.PrimaryForecast,
            FullYearPWForecast: hotel.FullYear_GOP.PrevWeekForecast,
            FullYearPWVariance: (hotel.FullYear_GOP.PrimaryForecast - hotel.FullYear_GOP.PrevWeekForecast) / hotel.FullYear_GOP.PrevWeekForecast
        }

        await saveFuncs.saveGopObj(gopObj);

        // NOI
        var noiObj = { 
            HotelId: hotel.HotelId, 
            ReportId: reportId, 
            MTD: hotel.MTD_AM_Stats ? hotel.MTD_AM_Stats.NOI : null, 
            YTD: hotel.YTD_AM_Stats ? hotel.YTD_AM_Stats.NOI : null, 
            FullMonthForecast: hotel.Monthly_NOI.PrimaryForecast,
            FullMonthPWForecast: hotel.Monthly_NOI.PrevWeekForecast,
            FullMonthPWVariance: (hotel.Monthly_NOI.PrimaryForecast - hotel.Monthly_NOI.PrevWeekForecast) / hotel.Monthly_NOI.PrevWeekForecast,
            F3Forecast: hotel.F3_NOI.PrimaryForecast,
            F3PWForecast: hotel.F3_NOI.PrevWeekForecast,
            F3PWVariance: (hotel.F3_NOI.PrimaryForecast - hotel.F3_NOI.PrevWeekForecast) / hotel.F3_NOI.PrevWeekForecast,
            FullYearForecast: hotel.FullYear_NOI.PrimaryForecast,
            FullYearPWForecast: hotel.FullYear_NOI.PrevWeekForecast,
            FullYearPWVariance: (hotel.FullYear_NOI.PrimaryForecast - hotel.FullYear_NOI.PrevWeekForecast) / hotel.FullYear_NOI.PrevWeekForecast
        }

        await saveFuncs.saveNoiObj(noiObj);
    };
}

async function importData() {
    await pool.connect();
    var request = pool.request();
    var result = await request.query("Select Name, HotelId FROM Hotels");
    currentHotels = result.recordset;

    var sheetPromises = [];

    xlsxFile('./ProfitSwordExports.xlsx', { getSheets: true }).then((sheets) => {
        _(sheets).each((obj) => {
            // make a promise to read the sheet's row to know when that sheet is done reading
            var promise = xlsxFile('./ProfitSwordExports.xlsx', {sheet: obj.name}).then((rows) => {
                console.log("Read Sheet: " + obj.name);
                transformData(rows, obj.name);
            });
            sheetPromises.push(promise);
        });

        // Make sure all sheets are done reading before moving on
        Promise.all(sheetPromises).then((values) => {
            console.log("DONE READING ALL SHEETS");
            currentHotels = _(currentHotels).indexBy('Name');

            // Transfer data from the dataObj to match with corresponding hotels to form a hotel obj with own set of data
            // go through each data sheet
            _(dataObj).each((obj, sheetName) => {
                // going through array of sheet's data obj
                _(obj).each((data) => {
                    if (currentHotels[data.HotelName]) currentHotels[data.HotelName][sheetName] = data;
                });
            });
            
            // save data to DB
            saveData();
        });
    });
}

module.exports = importData;