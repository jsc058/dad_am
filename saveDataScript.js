var sql = require("mssql");
const pool = require("./connectDB.js");

function saveReportObj(reportObj) {
    var insertPromise = new Promise(async (resolve, reject) => {
        await pool.connect();
        var request = pool.request();
        
        request.input('SourceName', sql.NVarChar, reportObj.SourceName);
        request.input('ExtractDate', sql.DateTime, reportObj.ExtractDate);
        request.input('PreviousSnapshotWeek', sql.Date, reportObj.PreviousSnapshotWeek);
        request.query(
            'INSERT INTO Reports (SourceName, ExtractDate, PreviousSnapshotWeek) OUTPUT INSERTED.ReportId Values(@SourceName, @ExtractDate, @PreviousSnapshotWeek);',
            (err, result) => {
                if (err) {
                    console.log(err);
                    return reject();
                } 

                console.log("Done inserting");
                resolve(result.recordset);
            }
        );
    });

    return insertPromise;
}

function saveOccupancyObj(dataObj) {
    var insertPromise = new Promise(async (resolve, reject) => {
        await pool.connect();
        var request = pool.request();

        request.input('HotelId', sql.Int, dataObj.HotelId);
        request.input('ReportId', sql.Int, dataObj.ReportId);
        request.input('MTD', sql.Decimal(9,6), dataObj.MTD);
        request.input('YTD', sql.Decimal(9,6), dataObj.YTD);
        request.input('FullMonthForecast', sql.Decimal(9,6), dataObj.FullMonthForecast);
        request.input('FullMonthPWForecast', sql.Decimal(9,6), dataObj.FullMonthPWForecast);
        request.input('F3Forecast', sql.Decimal(9,6), dataObj.F3Forecast);
        request.input('F3PWForecast', sql.Decimal(9,6), dataObj.F3PWForecast);
        request.input('FullYearForecast', sql.Decimal(9,6), dataObj.FullYearForecast);
        request.input('FullYearPWForecast', sql.Decimal(9,6), dataObj.FullYearPWForecast);
        request.input('FullMonthPWVariance', sql.Decimal(19,17), dataObj.FullMonthPWVariance);
        request.input('F3PWVariance', sql.Decimal(19,17), dataObj.F3PWVariance);
        request.input('FullYearPWVariance', sql.Decimal(19,17), dataObj.FullYearPWVariance);

        request.query(
            'INSERT INTO Occupancy (HotelId, ReportId, MTD, YTD, FullMonthForecast, FullMonthPWForecast, F3Forecast, F3PWForecast, FullYearForecast, FullYearPWForecast, FullMonthPWVariance, F3PWVariance, FullYearPWVariance ) Values(@HotelId, @ReportId, @MTD, @YTD, @FullMonthForecast, @FullMonthPWForecast, @F3Forecast, @F3PWForecast, @FullYearForecast, @FullYearPWForecast, @FullMonthPWVariance, @F3PWVariance, @FullYearPWVariance);',
            (err, result) => {
                if (err) {
                    console.log(err);
                    return reject();
                } 

                console.log("Done inserting occObj from: " + dataObj.HotelId);
                resolve();
            }
        );
    });

    return insertPromise;
}

function saveADRObj(dataObj) {
    var insertPromise = new Promise (async (resolve, reject) => {
        await pool.connect();
        var request = pool.request();
    
        request.input('HotelId', sql.Int, dataObj.HotelId);
        request.input('ReportId', sql.Int, dataObj.ReportId);
        request.input('MTD', sql.Decimal(19,9), dataObj.MTD);
        request.input('YTD', sql.Decimal(19,9), dataObj.YTD);
        request.input('FullMonthForecast', sql.Decimal(19,9), dataObj.FullMonthForecast);
        request.input('FullMonthPWForecast', sql.Decimal(19,9), dataObj.FullMonthPWForecast);
        request.input('F3Forecast', sql.Decimal(19,9), dataObj.F3Forecast);
        request.input('F3PWForecast', sql.Decimal(19,9), dataObj.F3PWForecast);
        request.input('FullYearForecast', sql.Decimal(19,9), dataObj.FullYearForecast);
        request.input('FullYearPWForecast', sql.Decimal(19,9), dataObj.FullYearPWForecast);
        request.input('FullMonthPWVariance', sql.Decimal(19,17), dataObj.FullMonthPWVariance);
        request.input('F3PWVariance', sql.Decimal(19,17), dataObj.F3PWVariance);
        request.input('FullYearPWVariance', sql.Decimal(19,17), dataObj.FullYearPWVariance);

        request.query(
            'INSERT INTO ADR (HotelId, ReportId, MTD, YTD, FullMonthForecast, FullMonthPWForecast, F3Forecast, F3PWForecast, FullYearForecast, FullYearPWForecast, FullMonthPWVariance, F3PWVariance, FullYearPWVariance ) Values(@HotelId, @ReportId, @MTD, @YTD, @FullMonthForecast, @FullMonthPWForecast, @F3Forecast, @F3PWForecast, @FullYearForecast, @FullYearPWForecast, @FullMonthPWVariance, @F3PWVariance, @FullYearPWVariance);',
            (err, result) => {
                if (err) {
                    console.log(err);
                    return reject();
                } 

                console.log("Done inserting adrObj from: " + dataObj.HotelId);
                resolve();
                
            }
        );
    });

    return insertPromise;
}

function saveRevParObj(dataObj) {
    var insertPromise = new Promise(async (resolve, reject) => {
        await pool.connect();
        var request = pool.request();
    
        request.input('HotelId', sql.Int, dataObj.HotelId);
        request.input('ReportId', sql.Int, dataObj.ReportId);
        request.input('MTD', sql.Decimal(19,9), dataObj.MTD);
        request.input('YTD', sql.Decimal(19,9), dataObj.YTD);
        request.input('FullMonthForecast', sql.Decimal(19,9), dataObj.FullMonthForecast);
        request.input('FullMonthPWForecast', sql.Decimal(19,9), dataObj.FullMonthPWForecast);
        request.input('F3Forecast', sql.Decimal(19,9), dataObj.F3Forecast);
        request.input('F3PWForecast', sql.Decimal(19,9), dataObj.F3PWForecast);
        request.input('FullYearForecast', sql.Decimal(19,9), dataObj.FullYearForecast);
        request.input('FullYearPWForecast', sql.Decimal(19,9), dataObj.FullYearPWForecast);
        request.input('FullMonthPWVariance', sql.Decimal(19,17), dataObj.FullMonthPWVariance);
        request.input('F3PWVariance', sql.Decimal(19,17), dataObj.F3PWVariance);
        request.input('FullYearPWVariance', sql.Decimal(19,17), dataObj.FullYearPWVariance);

        request.query(
            'INSERT INTO RevPar (HotelId, ReportId, MTD, YTD, FullMonthForecast, FullMonthPWForecast, F3Forecast, F3PWForecast, FullYearForecast, FullYearPWForecast, FullMonthPWVariance, F3PWVariance, FullYearPWVariance ) Values(@HotelId, @ReportId, @MTD, @YTD, @FullMonthForecast, @FullMonthPWForecast, @F3Forecast, @F3PWForecast, @FullYearForecast, @FullYearPWForecast, @FullMonthPWVariance, @F3PWVariance, @FullYearPWVariance);',
            (err, result) => {
                if (err) {
                    console.log(err);
                    return reject();
                } 

                console.log("Done inserting revParObj from: " + dataObj.HotelId);
                resolve();
            }
        );
    });

    return insertPromise;
}

function saveTotalRevObj(dataObj) {
    var insertPromise = new Promise(async (resolve, reject) => {
        await pool.connect();
        var request = pool.request();
    
        request.input('HotelId', sql.Int, dataObj.HotelId);
        request.input('ReportId', sql.Int, dataObj.ReportId);
        request.input('MTD', sql.Decimal(19,7), dataObj.MTD);
        request.input('YTD', sql.Decimal(19,7), dataObj.YTD);
        request.input('FullMonthForecast', sql.Decimal(19,7), dataObj.FullMonthForecast);
        request.input('FullMonthPWForecast', sql.Decimal(19,7), dataObj.FullMonthPWForecast);
        request.input('F3Forecast', sql.Decimal(19,7), dataObj.F3Forecast);
        request.input('F3PWForecast', sql.Decimal(19,7), dataObj.F3PWForecast);
        request.input('FullYearForecast', sql.Decimal(19,7), dataObj.FullYearForecast);
        request.input('FullYearPWForecast', sql.Decimal(19,7), dataObj.FullYearPWForecast);
        request.input('FullMonthPWVariance', sql.Decimal(19,17), dataObj.FullMonthPWVariance);
        request.input('F3PWVariance', sql.Decimal(19,17), dataObj.F3PWVariance);
        request.input('FullYearPWVariance', sql.Decimal(19,17), dataObj.FullYearPWVariance);

        request.query(
            'INSERT INTO TotalRev (HotelId, ReportId, MTD, YTD, FullMonthForecast, FullMonthPWForecast, F3Forecast, F3PWForecast, FullYearForecast, FullYearPWForecast, FullMonthPWVariance, F3PWVariance, FullYearPWVariance ) Values(@HotelId, @ReportId, @MTD, @YTD, @FullMonthForecast, @FullMonthPWForecast, @F3Forecast, @F3PWForecast, @FullYearForecast, @FullYearPWForecast, @FullMonthPWVariance, @F3PWVariance, @FullYearPWVariance);',
            (err, result) => {
                if (err) {
                    console.log(err);
                    return reject();
                } 

                console.log("Done inserting totalRevObj from: " + dataObj.HotelId);
                resolve();
            }
        );
    });

    return insertPromise;
}

function saveGopObj(dataObj) {
    var insertPromise = new Promise(async (resolve, reject) => {
        await pool.connect();
        var request = pool.request();
    
        request.input('HotelId', sql.Int, dataObj.HotelId);
        request.input('ReportId', sql.Int, dataObj.ReportId);
        request.input('MTD', sql.Decimal(19,7), dataObj.MTD);
        request.input('YTD', sql.Decimal(19,7), dataObj.YTD);
        request.input('FullMonthForecast', sql.Decimal(19,7), dataObj.FullMonthForecast);
        request.input('FullMonthPWForecast', sql.Decimal(19,7), dataObj.FullMonthPWForecast);
        request.input('F3Forecast', sql.Decimal(19,7), dataObj.F3Forecast);
        request.input('F3PWForecast', sql.Decimal(19,7), dataObj.F3PWForecast);
        request.input('FullYearForecast', sql.Decimal(19,7), dataObj.FullYearForecast);
        request.input('FullYearPWForecast', sql.Decimal(19,7), dataObj.FullYearPWForecast);
        request.input('FullMonthPWVariance', sql.Decimal(19,17), dataObj.FullMonthPWVariance);
        request.input('F3PWVariance', sql.Decimal(19,17), dataObj.F3PWVariance);
        request.input('FullYearPWVariance', sql.Decimal(19,17), dataObj.FullYearPWVariance);

        request.query(
            'INSERT INTO GOP (HotelId, ReportId, MTD, YTD, FullMonthForecast, FullMonthPWForecast, F3Forecast, F3PWForecast, FullYearForecast, FullYearPWForecast, FullMonthPWVariance, F3PWVariance, FullYearPWVariance ) Values(@HotelId, @ReportId, @MTD, @YTD, @FullMonthForecast, @FullMonthPWForecast, @F3Forecast, @F3PWForecast, @FullYearForecast, @FullYearPWForecast, @FullMonthPWVariance, @F3PWVariance, @FullYearPWVariance);',
            (err, result) => {
                if (err) {
                    console.log(err);
                    return reject();
                } 

                console.log("Done inserting gopObj from: " + dataObj.HotelId);
                resolve();
            }
        );


    });

    return insertPromise;
}

function saveNoiObj(dataObj) {
    var insertPromise = new Promise(async (resolve, reject) => {
        await pool.connect();
        var request = pool.request();
    
        request.input('HotelId', sql.Int, dataObj.HotelId);
        request.input('ReportId', sql.Int, dataObj.ReportId);
        request.input('MTD', sql.Decimal(19,7), dataObj.MTD);
        request.input('YTD', sql.Decimal(19,7), dataObj.YTD);
        request.input('FullMonthForecast', sql.Decimal(19,7), dataObj.FullMonthForecast);
        request.input('FullMonthPWForecast', sql.Decimal(19,7), dataObj.FullMonthPWForecast);
        request.input('F3Forecast', sql.Decimal(19,7), dataObj.F3Forecast);
        request.input('F3PWForecast', sql.Decimal(19,7), dataObj.F3PWForecast);
        request.input('FullYearForecast', sql.Decimal(19,7), dataObj.FullYearForecast);
        request.input('FullYearPWForecast', sql.Decimal(19,7), dataObj.FullYearPWForecast);
        request.input('FullMonthPWVariance', sql.Decimal(19,17), dataObj.FullMonthPWVariance);
        request.input('F3PWVariance', sql.Decimal(19,17), dataObj.F3PWVariance);
        request.input('FullYearPWVariance', sql.Decimal(19,17), dataObj.FullYearPWVariance);

        request.query(
            'INSERT INTO NOI (HotelId, ReportId, MTD, YTD, FullMonthForecast, FullMonthPWForecast, F3Forecast, F3PWForecast, FullYearForecast, FullYearPWForecast, FullMonthPWVariance, F3PWVariance, FullYearPWVariance ) Values(@HotelId, @ReportId, @MTD, @YTD, @FullMonthForecast, @FullMonthPWForecast, @F3Forecast, @F3PWForecast, @FullYearForecast, @FullYearPWForecast, @FullMonthPWVariance, @F3PWVariance, @FullYearPWVariance);',
            (err, result) => {
                if (err) {
                    console.log(err);
                    return reject();
                }
                
                console.log("Done inserting noiObj from: " + dataObj.HotelId);
                resolve();
                
            }
        );
    });

    return insertPromise;
}

module.exports = {
    saveReportObj: saveReportObj,
    saveOccupancyObj: saveOccupancyObj,
    saveADRObj: saveADRObj,
    saveRevParObj: saveRevParObj,
    saveTotalRevObj: saveTotalRevObj,
    saveGopObj: saveGopObj,
    saveNoiObj: saveNoiObj
};