var sql = require("mssql");
require('dotenv').config();

var config = {
    server: process.env.SQLDB_SERVER,
    user: process.env.SQLDB_USERNAME,
    password: process.env.SQLDB_PASSWORD,
    database: process.env.SQLDB_DATABASE,
    options: {
        encrypt: true
    }
};

const pool = new sql.ConnectionPool(config);
//const poolConnect = pool.connect();

module.exports = pool;




