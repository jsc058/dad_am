var monthlyForecastJoin = `Select H.Name, O.MTD as OccMTD, O.FullMonthForecast as OccFM, O.FullMonthPWForecast as OccPWFM,
                        A.MTD as AdrMTD, O.FullMonthForecast as AdrFM, O.FullMonthPWForecast as AdrPWFM, 
                        R.MTD as RevParMTD, R.FullMonthForecast as RevParFM, R.FullMonthPWForecast as RevParPWFM,
                        T.MTD as TotalRevMTD, T.FullMonthForecast as TotalRevFM, T.FullMonthPWForecast as TotalRevPWFM,
                        G.MTD as GopMTD, G.FullMonthForecast as GopFM, G.FullMonthPWForecast as GopPWFM,
                        N.MTD as NoiMTD, N.FullMonthForecast as NoiFM, N.FullMonthPWForecast as NoiPWFM,
                        P.ExtractDate From Hotels H
                        Join Occupancy O On H.HotelId = O.HotelId 
                        Join ADR A On H.HotelId = A.HotelId
                        Join RevPar R On H.HotelId = R.HotelId
                        Join TotalRev T On H.HotelId = T.HotelId
                        Join GOP G On H.HotelId = G.HotelId
                        Join NOI N On H.HotelId = N.HotelId
                        Join Reports P On O.ReportId = P.ReportId And A.ReportId = P.ReportId And R.ReportId = P.ReportId
                            And T.ReportId = P.ReportId And G.ReportId = P.ReportId And N.ReportId = P.ReportId
                            Where P.ReportId = 9`;
                            //Where P.ExtractDate = (Select max(ExtractDate) From Reports)`;
                            // to fix bug caused by scraper...
var forward3ForecastJoin = `Select H.Name, O.F3Forecast as OccF3, O.F3PWForecast as OccPWF3,
                        O.F3Forecast as AdrF3, O.F3PWForecast as AdrPWF3, 
                        R.F3Forecast as RevParF3, R.F3PWForecast as RevParPWF3,
                        T.F3Forecast as TotalRevF3, T.F3PWForecast as TotalRevPWF3,
                        G.F3Forecast as GopF3, G.F3PWForecast as GopPWF3,
                        N.F3Forecast as NoiF3, N.F3PWForecast as NoiPWF3,
                        P.ExtractDate From Hotels H
                        Join Occupancy O On H.HotelId = O.HotelId 
                        Join ADR A On H.HotelId = A.HotelId
                        Join RevPar R On H.HotelId = R.HotelId
                        Join TotalRev T On H.HotelId = T.HotelId
                        Join GOP G On H.HotelId = G.HotelId
                        Join NOI N On H.HotelId = N.HotelId
                        Join Reports P On O.ReportId = P.ReportId And A.ReportId = P.ReportId And R.ReportId = P.ReportId
                            And T.ReportId = P.ReportId And G.ReportId = P.ReportId And N.ReportId = P.ReportId
                            Where P.ReportId = 9`;
                            //Where P.ExtractDate = (Select max(ExtractDate) From Reports)`;    

var fullYearForecastJoin = `Select H.Name, O.YTD as OccYTD, O.FullYearForecast as OccFY, O.FullYearPWForecast as OccPWFY,
                        A.YTD as AdrYTD, O.FullYearForecast as AdrFY, O.FullYearPWForecast as AdrPWFY, 
                        R.YTD as RevParYTD, R.FullYearForecast as RevParFY, R.FullYearPWForecast as RevParPWFY,
                        T.YTD as TotalRevYTD, T.FullYearForecast as TotalRevFY, T.FullYearPWForecast as TotalRevPWFY,
                        G.YTD as GopYTD, G.FullYearForecast as GopFY, G.FullYearPWForecast as GopPWFY,
                        N.YTD as NoiYTD, N.FullYearForecast as NoiFY, N.FullYearPWForecast as NoiPWFY,
                        P.ExtractDate From Hotels H
                        Join Occupancy O On H.HotelId = O.HotelId 
                        Join ADR A On H.HotelId = A.HotelId
                        Join RevPar R On H.HotelId = R.HotelId
                        Join TotalRev T On H.HotelId = T.HotelId
                        Join GOP G On H.HotelId = G.HotelId
                        Join NOI N On H.HotelId = N.HotelId
                        Join Reports P On O.ReportId = P.ReportId And A.ReportId = P.ReportId And R.ReportId = P.ReportId
                            And T.ReportId = P.ReportId And G.ReportId = P.ReportId And N.ReportId = P.ReportId
                            Where P.ReportId = 9`;
                            //Where P.ExtractDate = (Select max(ExtractDate) From Reports)`;

const reportQueries = {
    MonthlyForecast: monthlyForecastJoin,
    Forward3Forecast: forward3ForecastJoin,
    FullYearForecast: fullYearForecastJoin
}                        

module.exports = reportQueries;