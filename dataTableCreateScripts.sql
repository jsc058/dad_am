CREATE TABLE Hotels (
    HotelId INT NOT NULL Primary Key Identity (1,1),
    Name NVARCHAR(100) NOT NULL,
    Portfolio NVARCHAR(100),
    IsDeleted BIT DEFAULT 'FALSE' NOT NULL
)

CREATE TABLE Reports (
    ReportId INT NOT NULL Primary Key Identity (1,1),
    SourceName NVARCHAR(100) NOT NULL,
    ExtractDate DATETIME NOT NULL,
    PreviousSnapshotWeek DATE
)

-- Occupancy is stored as a % 
-- Using decimal precison of 9 bc max precision for 5 storage bytes (lowest storage #) is 9
-- Assume that 0 <= occupancy >= 100, can guarantee a max 3 digits to the left of decimal
-- PS seems to only sotre 1 decimal to the right for occupancy %
-- Might be better to calculate variance on client side instead of saving that value to db
    -- Just add column for now 
    -- Calculation will result in a rate (can convert to % in client side) => don't need 
    -- too much precision on the left hand side
CREATE TABLE Occupancy (
    OccupancyId INT NOT NULL Primary Key Identity (1,1),
    HotelId INT NOT NULL FOREIGN KEY REFERENCES Hotels(HotelId),
    ReportId INT NOT NULL FOREIGN KEY REFERENCES Reports(ReportId),
    MTD DECIMAL(9,6),
    FullMonthForecast DECIMAL(9,6),
    FullMonthPWForecast DECIMAL(9,6),
    FullMonthPWVariance DECIMAL(19,17),
    F3Forecast DECIMAL(9,6),
    F3PWForecast DECIMAL(9,6),
    F3PWVariance DECIMAL(19,17),
    YTD DECIMAL(9,6),
    FullYearForecast DECIMAL(9,6),
    FullYearPWForecast DECIMAL(9,6),
    FullYearPWVariance DECIMAL(19,17),
)

-- The rest of the metrics are represented in dollar amounts
-- Assume that these #s can't reach over a billion dollars => 10 max places to the left
-- Storage bytes of 9 can store 10 - 19 precision
-- PS doesn't seem to store more than 2 digits of precision to the right of decimal
CREATE TABLE ADR (
    ADRId INT NOT NULL Primary Key Identity (1,1),
    HotelId INT NOT NULL FOREIGN KEY REFERENCES Hotels(HotelId),
    ReportId INT NOT NULL FOREIGN KEY REFERENCES Reports(ReportId),
    MTD DECIMAL(19,9),
    FullMonthForecast DECIMAL(19,9),
    FullMonthPWForecast DECIMAL(19,9),
    FullMonthPWVariance DECIMAL(19,17), 
    F3Forecast DECIMAL(19,9),
    F3PWForecast DECIMAL(19,9),
    F3PWVariance DECIMAL(19,17),
    YTD DECIMAL(19,9),
    FullYearForecast DECIMAL(19,9),
    FullYearPWForecast DECIMAL(19,9),
    FullYearPWVariance DECIMAL(19,17),
)

CREATE TABLE RevPar (
    RevParId INT NOT NULL Primary Key Identity (1,1),
    HotelId INT NOT NULL FOREIGN KEY REFERENCES Hotels(HotelId),
    ReportId INT NOT NULL FOREIGN KEY REFERENCES Reports(ReportId),
    MTD DECIMAL(19,9),
    FullMonthForecast DECIMAL(19,9),
    FullMonthPWForecast DECIMAL(19,9),
    FullMonthPWVariance DECIMAL(19,17), 
    F3Forecast DECIMAL(19,9),
    F3PWForecast DECIMAL(19,9),
    F3PWVariance DECIMAL(19,17),
    YTD DECIMAL(19,9),
    FullYearForecast DECIMAL(19,9),
    FullYearPWForecast DECIMAL(19,9),
    FullYearPWVariance DECIMAL(19,17),
)

-- PS not keeping decimal places for the TotalRev, GOP, NOI
-- Give more precision to the left hand side (for values that are extracted) => max 100 billion
-- maintain same precision for variance 
CREATE TABLE TotalRev (
    TotalRevId INT NOT NULL Primary Key Identity (1,1),
    HotelId INT NOT NULL FOREIGN KEY REFERENCES Hotels(HotelId),
    ReportId INT NOT NULL FOREIGN KEY REFERENCES Reports(ReportId),
    MTD DECIMAL(19,7),
    FullMonthForecast DECIMAL(19,7),
    FullMonthPWForecast DECIMAL(19,7),
    FullMonthPWVariance DECIMAL(19,17), 
    F3Forecast DECIMAL(19,7),
    F3PWForecast DECIMAL(19,7),
    F3PWVariance DECIMAL(19,17),
    YTD DECIMAL(19,7),
    FullYearForecast DECIMAL(19,7),
    FullYearPWForecast DECIMAL(19,7),
    FullYearPWVariance DECIMAL(19,17),
)

CREATE TABLE GOP (
    GOPId INT NOT NULL Primary Key Identity (1,1),
    HotelId INT NOT NULL FOREIGN KEY REFERENCES Hotels(HotelId),
    ReportId INT NOT NULL FOREIGN KEY REFERENCES Reports(ReportId),
    MTD DECIMAL(19,7),
    FullMonthForecast DECIMAL(19,7),
    FullMonthPWForecast DECIMAL(19,7),
    FullMonthPWVariance DECIMAL(19,17), 
    F3Forecast DECIMAL(19,7),
    F3PWForecast DECIMAL(19,7),
    F3PWVariance DECIMAL(19,17),
    YTD DECIMAL(19,7),
    FullYearForecast DECIMAL(19,7),
    FullYearPWForecast DECIMAL(19,7),
    FullYearPWVariance DECIMAL(19,17),
)

CREATE TABLE NOI (
    NOIId INT NOT NULL Primary Key Identity (1,1),
    HotelId INT NOT NULL FOREIGN KEY REFERENCES Hotels(HotelId),
    ReportId INT NOT NULL FOREIGN KEY REFERENCES Reports(ReportId),
    MTD DECIMAL(19,7),
    FullMonthForecast DECIMAL(19,7),
    FullMonthPWForecast DECIMAL(19,7),
    FullMonthPWVariance DECIMAL(19,17), 
    F3Forecast DECIMAL(19,7),
    F3PWForecast DECIMAL(19,7),
    F3PWVariance DECIMAL(19,17),
    YTD DECIMAL(19,7),
    FullYearForecast DECIMAL(19,7),
    FullYearPWForecast DECIMAL(19,7),
    FullYearPWVariance DECIMAL(19,17),
)


