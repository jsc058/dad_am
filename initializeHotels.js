const xlsxFile = require('read-excel-file/node');
const _ = require("underscore");
var sql = require("mssql");
const pool = require("./connectDB.js");

var currentHotels = [];

async function insertTable(data) {
    await pool.connect();
    var request = pool.request();
    request.input("Name", sql.NVarChar, data);
    var result = await request.query("Insert Into Hotels (Name) OUTPUT INSERTED.Name Values(@Name)");
}

async function initializeHotels() {
    await xlsxFile('./HotelsList.xlsx').then((rows) => {
        rows.forEach((row) => {
            currentHotels.push(row[0]);
            insertTable(row[0]);
        });
    });

    return currentHotels;
}

module.exports = initializeHotels;