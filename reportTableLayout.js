function showMonthlyReport (data) {
    var extractDate = data.recordset[0].ExtractDate;
    var displayDate = extractDate.getFullYear() + '-' + (extractDate.getMonth() + 1) + '-' + extractDate.getDate();
    let header = `<h1 style="background: pink"> Monthly Forecast Report ${displayDate} </h1>`
    let table = `<table border="1"> <thead>
        <tr> 
            <th>Hotel Name</th> <th colspan="3">Occupancy</th> <th colspan="3">ADR</th> <th colspan="3">RevPar</th> 
            <th colspan="3">TotalRevenue</th> <th colspan="3">GOP</th> <th colspan="3">NOI</th>
        <tr>
        <tr>
            <th></th> <th>MTD</th><th>Full Month</th><th>PW Variance</th> <th>MTD</th><th>Full Month</th><th>PW Variance</th> 
            <th>MTD</th><th>Full Month</th><th>PW Variance</th> <th>MTD</th><th>Full Month</th><th>PW Variance</th> 
            <th>MTD</th><th>Full Month</th><th>PW Variance</th> <th>MTD</th><th>Full Month</th><th>PW Variance</th>
        </tr>
    <thead>`;
    let row = '';

    for (let i = 0; i < data.recordset.length; i++) {
        var record = data.recordset[i];
        row = row + `<tr> 
            <td>${record.Name}</td> 
            <td>${record.OccMTD} %</td> 
            <td>${record.OccFM} %</td> 
            <td>${((record.OccFM-record.OccPWFM)/record.OccFM * 100).toFixed(3)} %</td> 
            <td>$ ${record.AdrMTD}</td> 
            <td>$ ${record.AdrFM}</td> 
            <td>${((record.AdrFM-record.AdrPWFM)/record.AdrFM * 100).toFixed(3)} %</td> 
            <td>$ ${record.RevParMTD}</td> 
            <td>$ ${record.RevParFM}</td> 
            <td>${((record.RevParFM-record.RevParPWFM)/record.RevParFM * 100).toFixed(3)} %</td> 
            <td>$ ${record.TotalRevMTD}</td> 
            <td>$ ${record.TotalRevFM}</td>
            <td>${((record.TotalRevFM-record.TotalRevPWFM)/record.TotalRevFM * 100).toFixed(3)} %</td> 
            <td>${record.GopMTD}</td> 
            <td>$ ${record.GopFM}</td> 
            <td>${((record.GopFM-record.GopPWFM)/record.GopFM * 100).toFixed(3)} %</td> 
            <td>$ ${record.NoiMTD}</td> 
            <td>$ ${record.NoiFM}</td> 
            <td>${((record.NoiFM-record.NoiPWFM)/record.NoiFM * 100).toFixed(3)} %</td> 
        </tr>`;
    }

    table = table + row + '</table>';
    return header + table;
}

function showF3Report (data) {
    var extractDate = data.recordset[0].ExtractDate;
    var displayDate = extractDate.getFullYear() + '-' + (extractDate.getMonth() + 1) + '-' + extractDate.getDate();
    let header = `<h1 style="background: lime"> Forward 3 Forecast Report ${displayDate} </h1>`
    let table = `<table border="1"> <thead>
        <tr> 
            <th>Hotel Name</th> <th colspan="2">Occupancy</th> <th colspan="2">ADR</th> <th colspan="2">RevPar</th> 
            <th colspan="2">TotalRevenue</th> <th colspan="2">GOP</th> <th colspan="2">NOI</th>
        <tr>
        <tr>
            <th></th> <th>Full Month</th><th>PW Variance</th> <th>Full Month</th><th>PW Variance</th> 
            <th>Full Month</th><th>PW Variance</th> <th>Full Month</th><th>PW Variance</th> 
            <th>Full Month</th><th>PW Variance</th> <th>Full Month</th><th>PW Variance</th>
        </tr>
    <thead>`;
    let row = '';

    for (let i = 0; i < data.recordset.length; i++) {
        var record = data.recordset[i];
        row = row + `<tr> 
            <td>${record.Name}</td> 
            <td>${record.OccF3} %</td> 
            <td>${((record.OccF3-record.OccPWF3)/record.OccF3 ).toFixed(3)} %</td> 
            <td>$ ${record.AdrF3}</td> 
            <td>${((record.AdrF3-record.AdrPWF3)/record.AdrF3 ).toFixed(3)} %</td> 
            <td>$ ${record.RevParF3}</td> 
            <td>${((record.RevParF3-record.RevParPWF3)/record.RevParF3 ).toFixed(3)} %</td> 
            <td>$ ${record.TotalRevF3}</td>
            <td>${((record.TotalRevF3-record.TotalRevPWF3)/record.TotalRevF3 ).toFixed(3)} %</td> 
            <td>$ ${record.GopF3}</td> 
            <td>${((record.GopF3-record.GopPWF3)/record.GopF3 ).toFixed(3)} %</td> 
            <td>$ ${record.NoiF3}</td> 
            <td>${((record.NoiF3-record.NoiPWF3)/record.NoiF3 ).toFixed(3)} %</td> 
        </tr>`;
    }

    table = table + row + '</table>';
    return header + table;
}

function showFullYearReport (data) {
    var extractDate = data.recordset[0].ExtractDate;
    var displayDate = extractDate.getFullYear() + '-' + (extractDate.getMonth() + 1) + '-' + extractDate.getDate();
    let header = `<h1 style="background: aqua"> Full Year Forecast Report ${displayDate} </h1>`
    let table = `<table border="1"> <thead>
        <tr> 
            <th>Hotel Name</th> <th colspan="3">Occupancy</th> <th colspan="3">ADR</th> <th colspan="3">RevPar</th> 
            <th colspan="3">TotalRevenue</th> <th colspan="3">GOP</th> <th colspan="3">NOI</th>
        <tr>
        <tr>
            <th></th> <th>YTD</th><th>Full Year</th><th>PW Variance</th> <th>YTD</th><th>Full Year</th><th>PW Variance</th> 
            <th>YTD</th><th>Full Year</th><th>PW Variance</th> <th>YTD</th><th>Full Year</th><th>PW Variance</th> 
            <th>YTD</th><th>Full Year</th><th>PW Variance</th> <th>YTD</th><th>Full Year</th><th>PW Variance</th>
        </tr>
    <thead>`;
    let row = '';

    for (let i = 0; i < data.recordset.length; i++) {
        var record = data.recordset[i];
        row = row + `<tr> 
            <td>${record.Name}</td> 
            <td>${record.OccYTD} %</td> 
            <td>${record.OccFY} %</td> 
            <td>${((record.OccFY-record.OccPWFY)/record.OccFY ).toFixed(3)} %</td> 
            <td>$ ${record.AdrYTD}</td> 
            <td>$ ${record.AdrFY}</td> 
            <td>${((record.AdrFY-record.AdrPWFY)/record.AdrFY ).toFixed(3)} %</td> 
            <td>$ ${record.RevParYTD}</td> 
            <td>$ ${record.RevParFY}</td> 
            <td>${((record.RevParFY-record.RevParPWFY)/record.RevParFY ).toFixed(3)} %</td> 
            <td>$ ${record.TotalRevYTD}</td> 
            <td>$ ${record.TotalRevFY}</td>
            <td>${((record.TotalRevFY-record.TotalRevPWFY)/record.TotalRevFY ).toFixed(3)} %</td> 
            <td>$ ${record.GopYTD}</td> 
            <td>$ ${record.GopFY}</td> 
            <td>${((record.GopFY-record.GopPWFY)/record.GopFY ).toFixed(3)} %</td> 
            <td>$ ${record.NoiYTD}</td> 
            <td>$ ${record.NoiFY}</td> 
            <td>${((record.NoiFY-record.NoiPWFY)/record.NoiFY ).toFixed(3)} %</td> 
        </tr>`;
    }

    table = table + row + '</table>';
    return header + table;
}

module.exports = {
    showMonthlyReport: showMonthlyReport,
    showF3Report: showF3Report,
    showFullYearReport: showFullYearReport
}