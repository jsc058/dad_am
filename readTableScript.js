
var connection = require("./connectDB.js");
const { read } = require("fs");
var Request = require('tedious').Request;
var TYPES = require('tedious').TYPES;
//var _ = require("underscore");


var currentHotels = [];
// grab list of crrent hotels
function readData() {
    console.log('Reading rows from the Table...');

    // Read all rows from table
    var readPromise = new Promise((resolve, reject) => {
        request = new Request(
            'SELECT * FROM DadAm.Hotels;',
            function(err, rowCount, rows) {
                if (err) {
                    console.log(err);
                    reject();
                } else {
                    // When done reading all rows
                    console.log(rowCount + ' row(s) returned');
                    //importData();
                    resolve(currentHotels);
                }
        });
    });    
    

    // add each hotel to list of currentHotels
    request.on('row', function(column) {
        // only selecting the Name so can assume grabbing the first and only val of column array
        var hotelName = column[0].value;
        var hotelId = column[1].value;
        // if name has x or temp#- in front, exclude (just for now, exclude in real extract)
        var regex1 = RegExp("^[x][A-Z]");
        var regex2 = RegExp("^Temp[0-9]");
        var regex3 = RegExp("^Temp-");
        if (!regex1.test(hotelName) && !regex2.test(hotelName) && !regex3.test(hotelName)) {
            currentHotels.push({hotelName, hotelId});
        }
    });

    // Execute SQL statement
    connection.execSql(request);

    return readPromise;
}

module.exports = readData;